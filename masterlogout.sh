#! /bin/sh

# generic iHook login script
#
# runs everything in /etc/hooks beginning with LI (for LogIn),
# similar in design to the SysV rc scripts.

export PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin

HOOKSDIR="/etc/hooks"

# become front application, disable UI and show barberpole
echo %BACKGROUND ${RAIHOOKBACKGROUND}
echo %WINDOWLEVEL HIGH
echo %BECOMEKEY
echo %UIMODE AUTOCRATIC
echo %BEGINPOLE

if [ -d ${HOOKSDIR} ]; then
    for hook in ${HOOKSDIR}/LO*; do
	if [ -s ${hook} -a -x ${hook} ]; then
	    logger -s -t LoginHook -p user.info Executing ${hook}... 1>&2

	    # run the item
	    ${hook} $*

	    if [ $? -ne 0 ]; then
		exit_value=$?
		logger -s -t LoginHook -p user.info ${hook} failed! 1>&2
		exit $exit_value
	    fi
	fi
    done
fi

echo LoginHook complete.
echo %ENDPOLE

sleep 1

exit 0
